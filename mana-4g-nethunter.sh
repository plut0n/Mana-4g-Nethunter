#!/bin/bash

#with wifi and 4g activated by default

MANA_SCRIPT='/usr/share/mana-toolkit/run-mana/start-nat-full-lollipop.sh'

rfkill block 1 #block wifi on wlan0
ifconfig wlan1 up #start the usb wireless card

sed -i '/upstream=wlan0/c\upstream=rmnet0' $MANA_SCRIPT #rmnet0=4g
bash $MANA_SCRIPT

sed -i '/upstream=rmnet0/c\upstream=wlan0' $MANA_SCRIPT #set as it was before
rfkill unblock 1 #will re-activate the wifi